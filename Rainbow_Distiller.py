#!/usr/bin/env python
# Soley written by Samantha Taffner
# Rainbow_Distiller Program started as HiCpy 
# version 1 started May 26th, 2013
# version 4 updated Jan 20th 2016 Program name changed to Rainbow HiC
# version 5 rewrote Feb 23rd 2016

# import sys
import os #get current directory
import argparse
import multiprocessing as mp
import sys
import shutil
from time import gmtime, strftime
import collections
import itertools
from distutils.dir_util import copy_tree
import re
import operator
#import psutil  Maybe use to see how much ram is on users computer

version = 'Rainbow HiC v0.4'
current_Progress = -1
total_Steps = 6
class MapReduce(object):
	'''Purpose: bin data
	'''
	def __init__(self, mapFunction, reduceFunction, numWorkers, typeData):
		"""
		mapFunction

		Function to map inputs to intermediate data. Takes as
		argument one input value and returns a tuple with the key
		and a value to be reduced.
			
		reduceFunction

		Function to reduce partitioned version of intermediate data
		to final output. Takes as argument a key as produced by
		map_func and a sequence of the values associated with that
		key.

		numWorkers

		Number of processors to send to
		(function, function, int) --> make MapReduce object
		"""
		self.mapFunction = mapFunction 
		self.reduceFunction = reduceFunction
		self.pool = mp.Pool(numWorkers)
		self.typeData = typeData 
	def partition(self, mappedValues):
		'''Purpose: Organize the bin pairs by their key.
			Returns an unsorted sequence of tuples with a key and a sequence values.
			(list of tuples) --> iterator of list of tuples
		'''
		#make a dict where values are lists
		partitionedData = collections.defaultdict(list)
		#iterate thru all bin pairs and and to dict
		for key, value in mappedValues:
			partitionedData[key].append(value)
		#return an iterator of list of tuples
		return partitionedData.items()
	def __call__(self, inputFileList, chunksize=1):
		"""Process the inputFileList through the map and reduce functions given.

		inputFileList
		An iterable containing the input data to be processed.

		chunksize=1
		The portion of the input data to hand to each worker.  This
		can be used to tune performance during the mapping phase.
		"""
		#I think this is sending to the pool object the mapFunction, input files, and chunksize? map is a function of pool
		print '\nRainbow HiC User Update:\n\tMapping ' + self.typeData 
		mapResponses = self.pool.map(self.mapFunction, inputFileList,  chunksize=chunksize)
		#example mapResponses ---> [[('6_1586', '6_1680'), ('6_1493', '6_2625'), ('6_3697', '6_3693'), ('6_2525', '6_2499'), ('6_1960', '6_2516'), ('6_1145', '6_1566')]]
		if mapResponses[0] != None:
			print '\nRainbow HiC User Update:\n\tPartitioning '+ self.typeData 
			partitionedData = self.partition(itertools.chain(*mapResponses))
			del mapResponses
			#example partitionedData ---> [('6_1145', ['6_1566']), ('6_2525', ['6_2499']), ('6_1960', ['6_2516']), ('6_3697', ['6_3693']), ('6_1586', ['6_1680']), ('6_1493', ['6_2625'])]
			print '\nRainbow HiC User Update:\n\tReducing '+ self.typeData 
			self.pool.map(self.reduceFunction, partitionedData)
def main(args):
	'''purpose:  Runs the program
		(list) --->

	'''
	#set global variables

	declareGlobals(args)
	global map_Reduce_Temp_Folder, pair_dict, Dict_Keys, size_of_Amp, len_Dict, chr_Length_Dict


	#Check to see if input files are correctd
	correctFormat, size_of_Amp, chr_Length_Dict = fileFormatCheck(args)
	#check to see if args.p is not greater than the number of processors of computer
	
	if correctFormat:
 		del correctFormat
		#make temp folders
		folder1Location, folder2Location, map_Reduce_Temp_Folder, cisMappingFile = makeFilesFolders(args, 1)

		#split up both input files into files associated with chromosome placed in temp folder
		print '\nREADING FILE IN PARALLEL\n\tRainbow HiC User Update:\n\t\tNow reading your input files'

		processes = [mp.Process(target=splitInputFiles, args=(args.samFile1, folder1Location)), mp.Process(target=splitInputFiles, args=(args.samFile2, folder2Location))]
		
		runInParallel(processes) #split files in parallel
		#clear memory and hard drive of data not needed anymore
		del processes

		progress_bar()
	#pair cis 
		#load chromsome of interest in memory from first folder
		print '\nCIS PAIRING STARTED'

		pair_dict, Dict_Keys, len_Dict = cisPair(folder1Location, folder2Location, cisMappingFile, args)
		progress_bar()
		
		print '\nTRANS PAIRING STARTED\n\tRainbow HiC User Update:\n\t\tTrans pairing started.'


	#Trains pairing
		#pair folder2 trans against pair_dict
		numWorkers = numProcessorsLocally(int(args.p))
		transPairInit(folder2Location, numWorkers)

		#load pair_dict from folder 2
		#open folder 2 chromosome of interest file
		pair_dict = {}
		Dict_Keys = []
		
		of = open('%s%s.txt'%(folder2Location, args.chroNumInterest))
		pair_dict, Dict_Keys, len_Dict = makeStoreDict(of)
		of.close()
		del of

		print '\nRainbow HiC RAM Update:\n\t- Storage size of StoreDict is %s bytes.  \n\t- There are %s elements in the Store Dict.  \n\t- That means each element is about %s bytes in size. \n\t- The size of keys list is %s bytes.'%(sys.getsizeof(pair_dict), len_Dict, (sys.getsizeof(pair_dict)/len_Dict),sys.getsizeof(Dict_Keys))
		

		#pair folder1 trans against pair_dict from folder2
		transPairInit(folder1Location, args.p)

		progress_bar()
	#clear memory and hard drive of data not needed anymore
		print '\nRainbow HiC User Update:\n\tDeleting pairing'


		del pair_dict
		del Dict_Keys 
		del len_Dict
		deleteTempFiles(folder1Location)
		deleteTempFiles(folder2Location)
		
	#bin data 
		print '\nRainbow HiC User Update:\n\tBinning Data'

		#start with cis data
		#__init__ MapReduce for the cis file
		cisMapReducer = MapReduce(cisMapperFunction, cisReducerFunction, 1, 'cis Data')
		cisMapReducer([cisMappingFile])	
		progress_bar()
		#delete cisMapReducer object
		del cisMapReducer
		#start with Trans binning
		transMapReducer = MapReduce(transMapperFunction, transReducerFunction, numWorkers, 'trans Data')

		#get all files from map_Reduce_Temp_Folder trans folder
		fileDir = '%s/trans'%(map_Reduce_Temp_Folder)
		transFiles = [fileDir + '/' + f for f in os.listdir(fileDir)]
		
		#send trans file list to Trans mapper object
		transMapReducer(transFiles)
		progress_bar()
		del transMapReducer

	#start making website folder
		print '\nRainbow HiC User Update:\n\tMaking Website Folder'
		
		## copy Rainbow_Distiller_tool_box folder
		outDataLOC = makeFilesFolders(args, 3)
		binSize = 1000 #binsize for Lines around hindiii sites
		#combine in one file hindiSites, geneData, wig data into one both file called sharedData.js
		#geta all genes in area
		print '\nRainbow HiC User Update:\n\tBuilding shared data JSON files.'
		
		
		binsList = buildSharedData(outDataLOC, args, binSize)
		print '\nRainbow HiC User Update:\n\tBuilding cis data JSON files.'
		
		#find out what bins each line in cis bed file falls in 
		#add maxIntensity here instead of gene file
		binsList, maxIntensity = makeLineJson(outDataLOC, args, binsList)
		
		#make binLocations JSON 
		# Example: var binLocations = [{"bin": "bin1","binl1": 128005394,"binl2": 128006394, "binedLines": ['id1', 'id2', 'id5997']},
		makeBinJson(outDataLOC, binsList, maxIntensity)
		print '\nRainbow HiC User Update:\n\tBuilding trans data JSON files.'
		progress_bar()
		#make transData.js
		#start with transChrInfo var.  Get this info from args.chrSizeFile
		makeTransChrInfo(outDataLOC, args)

		#add line info for trans
		makeTransLineJson(outDataLOC, args)

	#delete all map reduce temp files
		deleteTempFiles('%s/temp'%(args.o))
		deleteTempFiles('%s/tempBinnedresults'%(args.o))
####################################################################################
#Make Bed file functions 
####################################################################################
def fileFormatCheck(args):
	'''
    	This function will make sure the format is correct for all input files

	(list) ---> bool    	

    	#sam file input format:
	    	len(line[0]) = 7
	    	D99KWTL1:845:C214UACXX:6:1101:1404:2170 3:N:0:      -       chr15   33279215


	'''

	correctFormat = True
	of = open(args.samFile1, 'r')
	#skip header of file
	line = of.readline()
	while line[0] == '@':

		line = of.readline()
	# line to find format
	line = of.readline()
	tabSplitLine = line.split('\t')
	#make Sure file tab seperated

	#set size of pcr product
	#check to make sure format is correct len(tabSplitLine) >= 8 otherwise set size_of_Amp to 50
	if len(tabSplitLine) >= 8:
		setsizeOfAmp = len(tabSplitLine[9])
	else:
		setsizeOfAmp = 50

	if len(tabSplitLine) <= 3:
		correctFormat = False
		errorReportPrinting(line, '\tERROR OCCURED BECAUSE FILE THERE NEEDS TO BE AT LEAST 3 COLUMNS !!!!!!!!!!!!!', 'sam')
	#check to see if an int is in tabSplitLine 3
	if not tabSplitLine[3].isdigit():
		correctFormat = False
		errorReportPrinting(line, '\tERROR OCCURED COLUMN 4 NEEDS TO BE A NUMBER', 'sam')		

	#make sure 5 colons are present in begining first element of tab split
	if len(tabSplitLine[0].split(':')) != 7:
		correctFormat = False
		errorReportPrinting(line, '\tERROR OCCURED BECAUSE 5 COLONS ARE REQURED IN FIRST COLUMN !!!!!!!!!!!!!!', 'sam')
	
	#make sure chromsome num is the 3rd element in the tabSplitLine
	if not tabSplitLine[2].startswith('chr') and  not tabSplitLine[2].startswith('*'):
		correctFormat = False
		errorReportPrinting(line, '\tERROR OCCURED BECAUSE CHR IS REQUIRED IN 3 COLUMN !!!!!!!!!!!!!!', 'sam')
	of.close()

	#make sure chromsome file is correct
	#open file
	of = open(args.chrSizeFile, 'r')
	line = of.readline()
	
	#check to see if chromsome file is correct format and if so make a chrLengthDict
	chrLengthDict = {}
	while line != '':
		
		tabSplitLine = line.rstrip().split('\t')
		
		if tabSplitLine != ['']:
			#check to see if there are three tab seperated columns
			if len(tabSplitLine) != 3:
				correctFormat = False
				errorReportPrinting(line, '\tError occured because your chromsome file does not contain 3 tab separated columns!!!!!!!!!!!', 'chr')
				break
			
			#remove chr and add line to chrLengthDict
			chrNum = int(tabSplitLine[0].replace('chr', ''))
			startLOC = int(tabSplitLine[1])
			endLOC = int(tabSplitLine[2])
			chrLengthDict[chrNum] = [startLOC, endLOC]
		
		line = of.readline()
	of.close()

	#check each wig files
	if args.wigList != None:
		for wig in args.wigList:
			of = open(wig, 'r')
			line = of.readline()
			spaceSplitLine = line.split(' ') #'track', 'type=wiggle_0', 'name="K27_mm10_normalized_chr6"'
			#find which element has type=
			for i in spaceSplitLine:
				if i.startswith('type='):
					typeFile = i.split('=')
					if typeFile[1] != 'wiggle_0':
						correctFormat = False
						print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
						print '\tWig files need to be type=wiggle_0'
			of.close()

	#check for fasta format
	# of = open(args.seqInterestRegion, 'r')

	#make sure file name ends with .fa
	if args.seqInterestRegion.split('.')[-1] != 'fa':
		correctFormat = False
		print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n' 	
		print '\tPlease make sure your region of interest is a .fa file'

	# line = of.readline()
	# commaSplitLine = line.split(',')
	
	


	# if len(commaSplitLine) != 3 or commaSplitLine[1] != 'HindIII' or commaSplitLine[2]!= 'AAGCTT\n':
	# 	correctFormat = False
	# 	print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
	# 	print '\tPlease use the HindIII cutter program supplied with Rainbow HiC'

	# of.close()

	#check GTF File
	gf =open(args.gtfFile, 'r')
	line = gf.readline()
	tabSplitLine = line.split('\t')
	#make sure GTF file has 9 columns
	if len(tabSplitLine) != 9:
		correctFormat = False
		print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
		print '\tPlease make sure your gtf file format has 9 columns'

	#make sure first column has chr in it
	if not tabSplitLine[0].startswith('chr'):
		correctFormat = False
		print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
		print '\tPlease make sure the first column of your gtf file contains chr\n\n\t\tExample: chr1'
	#make sure tabSplitLine[3] and tabSplitLine[4] are ints 
	if not tabSplitLine[3].isdigit() or not tabSplitLine[4].isdigit():
		correctFormat = False	                                               
		print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
		print '\tPlease make sure the 4th coloumn and 5th column are integers in the gtf file.'

	gf.close()
	#remove genes file
	#See if args.removeGenesFile is not equal to False
	if args.removeGenesFile != None:
		of = open(args.removeGenesFile, 'r')
		line =of.readline()
		
		while line != '':
			tabSplitLine = line.split('\t')
			#make sure it has two columns
			if len(tabSplitLine) != 2:
				correctFormat = False
				print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
				print '\tPlease Make sure your removeGenesFile file has 2 colums.'
				print '\texample input:\n\t\tAK	StartsWith\n\t\tRik	EndsWith\n\t\tKlra18	Gene'
				print '\tYour data looks like %s'%(line)
				break
			#make sure second column only contains StartsWith, EndsWith, Gene
			
			if tabSplitLine[1].rstrip() not in ['StartsWith', 'EndsWith', 'Gene']:
				correctFormat = False
				print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
				print '\tPlease make sure the second column of the removeGenesFile file only contains:\n\tStartsWith, EndsWith, Gene.'
				print '\texample input:\n\t\tAK	StartsWith\n\t\tRik	EndsWith\n\t\tKlra18	Gene'
				print '\tYour data looks like %s'%(line)
				break
			line =of.readline()
		of.close()
	return correctFormat, setsizeOfAmp, chrLengthDict
def errorReportPrinting(line, specficError, fileCheckType):
	'''purpose: print error for fileFormatCheck'''


	print 'RAINBOW HIC INPUT ERROR: Your file format does not meet file requirements\n'
	
	if fileCheckType == 'sam':
		print '\tRainbow HiC Step1.py requires reads to have a format like this\n'
		print '\tHW-ST997:514:H8ULNADXX:1:1101:1425:2107	16	chr3	115101327	etc..\n'
	elif fileCheckType == 'chr':
		print '\tRainbow HiC Step1.py requires the chromsome file to have a format like this\n'
		print '\tchr1	0	195000000\n'
	print '\tYour data looks like:\n\t%s'%(line)
	print specficError
	print '_'*100
def makeFilesFolders(args,step):
	'''Purpose: Make all files and folders for running
		folder structure created
			temp
				first file folder
				second file folder
				mapReduceTempResults 
					cisMappingFile.bed
					trans1.bed
					trans2.bed
					etc..
		(args) ---> 3 folders multiple files 
	'''
	#make all temp files/folders for cis 
	if step==1:
		#get current time for adding to folder
		currentTime = strftime('%H_%M_%S', gmtime())
		#get file names
		firstFileName = args.samFile1.split('/')[-1].split('.')[0]
		secondFileName = args.samFile2.split('/')[-1].split('.')[0]

		#make temp folders
		folder1Location = '%s/temp/%s_temp_%s/'%(args.o, firstFileName, currentTime)
		if not os.path.exists(folder1Location): os.makedirs(folder1Location)

		folder2Location = '%s/temp/%s_temp_%s/'%(args.o, secondFileName, currentTime)
		if not os.path.exists(folder2Location): os.makedirs(folder2Location)

		map_Reduce_Temp_Folder = '%s/temp/mapReduceTempResults_%s'%(args.o, currentTime)
		if not os.path.exists(map_Reduce_Temp_Folder): os.makedirs(map_Reduce_Temp_Folder)

		map_reduce_trans_folder = '%s/trans'%(map_Reduce_Temp_Folder)
		if not os.path.exists(map_reduce_trans_folder): os.makedirs(map_reduce_trans_folder)

		#make file for cis Mapping 
		cisMappingFile = '%s/cisMappingFile.bed'%(map_Reduce_Temp_Folder)
		makeFile = open(cisMappingFile, 'w')
		makeFile.close()
		return folder1Location, folder2Location, map_Reduce_Temp_Folder, cisMappingFile

	#declareGlobals called: makes all results files and folders
	elif step == 2:
		toMakeResultsFolder = '%s/tempBinnedresults'%(args.o)
		if not os.path.exists(toMakeResultsFolder): os.makedirs(toMakeResultsFolder)
		#make dict of all open results files
		fileDict = {}
		#add all trans data files
		for i in range(1,20):
			key = '%s'%(i)
			value = open('%s/%sand%s_trans.bed'%(toMakeResultsFolder, i, chro_num_interest), 'w')
			value.close()
			fileDict[key] = '%s/%sand%s_trans.bed'%(toMakeResultsFolder, i, chro_num_interest)
		#add all cis data files
		key = 'cis'
		value = open('%s/%sand%s_cis.txt'%(toMakeResultsFolder, chro_num_interest, chro_num_interest), 'w')
		value.close()
		fileDict[key] = '%s/%sand%s_cis.txt'%(toMakeResultsFolder, chro_num_interest, chro_num_interest)		
		return toMakeResultsFolder,fileDict 

	#make website out folder
	elif step == 3:
		outFileLOC = args.o + '/' + args.projectName #get a name for output website
		copy_tree(os.getcwd()+ '/tool_box_Rainbow_Distiller', outFileLOC)

		#start with cis data
		outDataLOC = outFileLOC + '/Tool_Box/js/data'

		#add a file for trans data, shared Data, and cis data
		#trans file
		mf = open(outDataLOC + '/transData.js', 'w')
		mf.close()

		#cis file
		mf = open(outDataLOC + '/cisData.js', 'w')
		mf.close()

		#Shared Data
		mf = open(outDataLOC + '/sharedData.js', 'w')
		mf.close()

		return outDataLOC
def splitInputFiles(inputFile, folderLOC):
	'''Purpose: This function will seperate a file reads into files for chromosome number
	'''
	#vars

	fileDict = {} #for saveing all open files

	#make files for each numbered chromosome
	for i in range(1, 20):
		key = '%s'%(i) #number between 1 and 19
		value = open('%s%s.txt'%(folderLOC, i), 'w')
		fileDict[key] = value

	strListChrNums = map(str,range(1,20)) #list of str ints from 1 to 19  

	#read thru input file put read with corresponding chr
	of = open(inputFile, 'r')
	#skip header
	line = of.readline()
	while line[0] == '@':

		line = of.readline()

	#start spliting file
	line = of.readline()

	while line != '':

		tabSplitLine = line.split('\t')
		chroNum = tabSplitLine[2].strip('chr')

		if not chroNum == '*':
			#remove chromosomes not between 1 and 19
			#write date to chromosome number file
			if chroNum in strListChrNums:
				fileDict[chroNum].write(line)



		line = of.readline()
	#done spliting file update user
	print '\n\tRainbow Hic User Update:\n\t\tAll done reading input files %s'%(inputFile)
	
	#close all files
	for key in fileDict:
		fileDict[key].close()

	of.close()
def runInParallel(processes):
	'''Purpose: starts processes in to run in parallel'''

	#running in parallel
	#run process
	for p in processes:
		p.start()
	#exit the completed processes
	for p in processes:
		p.join()
def cisPair(folder1Location, folder2Location, cisMappingFile, args):
	'''Purpose: 
	(str, str, str, list) -->
	'''
	print '\n\tRainbow HiC User Update:\n\t\tPreparing cis data.'
	#make a storeDict
	print '\n\tRainbow HiC User Update:\n\t\tStoring dict'

	storeFile = open('%s%s.txt'%(folder1Location, args.chroNumInterest))
	storeDict, sortedStoreDictKeys, lenStoreDict = makeStoreDict(storeFile)
	storeFile.close()
	print '\n\tRainbow HiC RAM Update:\n\t\t- Storage size of StoreDict is %s bytes.  \n\t\t- There are %s elements in the Store Dict.  \n\t\t- That means each element is about %s bytes in size. \n\t\t- The size of keys list is %s bytes.'%(sys.getsizeof(storeDict), len(storeDict.keys()), (sys.getsizeof(storeDict)/len(storeDict.keys()) ),sys.getsizeof(sortedStoreDictKeys))

	#read matching file and see if in storeDict
	print '\n\tRainbow HiC User Update:\n\t\tmatching with store dict'

	readFile = open(('%s%s.txt'%(folder2Location, args.chroNumInterest)))
	readFileAndMatch(storeDict, sortedStoreDictKeys, readFile, lenStoreDict, cisMappingFile)
	readFile.close()
	
	return storeDict, sortedStoreDictKeys, lenStoreDict
def makeStoreDict(of):
	'''Purpose: Store all data in one chromsome file to a dict
		(file) --> dict, sorted list list
		Example Dict output:
			{'59972116': ['128745582', '0', 'chr6\t128745547\t128745582', '59972116'],..

	'''
	#vars
	storeDict = {}
	line = of.readline()
	
	#read thru storing file
	while line != '':
		tabSplitLine = line.strip().split('\t')

		#get .bed format for storeDict
		xy, value = getAllInfoFromReadLine(tabSplitLine)

		#add to Store dict 
		#key: xy
		#value :chromsome number sign and bed format
		storeDict[xy] = value

		line = of.readline()

	storeDictKeys = storeDict.keys()
	storeDictKeys.sort()

	return storeDict, storeDictKeys, len(storeDict)
def getAllInfoFromReadLine(tabSplitLine):
	'''Purpose: get all relevent info from read line
		(list, int) --> str(int), list
		Example Input:
			Example tabSplitLine --> ['HW-ST997:514:H8ULNADXX:1:1101:10148:2076', '0', 'chr6', '25676532', '255', '50M', '*', '0', '0', 'CTCAAAGATCTCTCTTTGAAACAATGTTCTGCTGTTCTTTGTATCTCAGA', 'C@CFFFDDBHGGDHIIIHHIJJJGGBHHIIAFHIIEHJJJHGEGGIGEHG', 'XA:i:0', 'MD:Z:50', 'NM:i:0']
			Example size of amp --> 50
		Example output:
			Example xy --> 101482076
			Example list ---> [25676532, '0', 'chr6\t25676482\t25676532', 101482076, 'chr6', 25676482]

	'''

	chroNum = tabSplitLine[2]
	position = int(tabSplitLine[3])
	sign = tabSplitLine[1]

	#if on the reverse strand
	if sign == '16' or sign == '-':
		endingPos = position + size_of_Amp
		bed = '%s\t%s\t%s'%(chroNum, position, endingPos)
	#if on the forward strand
	elif sign == '0' or sign == '+':
		endingPos = position - size_of_Amp
		bed = '%s\t%s\t%s'%(chroNum, endingPos, position)

	#get xy coor on CHIP location
	queryNameList = tabSplitLine[0].split(' ')[0].split(':')
	xy = queryNameList[-2] + queryNameList[-1]

	return int(xy), [position, sign, bed, int(xy), chroNum, int(endingPos)] 
def readFileAndMatch(storeDict, sortedStoreDictKeys, of, lenStoreDict, writeFile):
	'''Purpose: match a file to what is in the storeDict.
		(dict, list, open file, int, int, str) --> write a file
		Example Inputs:
			storeDict --> { 39662079: [129462446, '0', 'chr6\t129462396\t129462446', 39662079, 'chr6', 129462396],..
			sortedStoreDictKeys --> [14222039, 14432056, 15502130,...
			of --> open file read file
			size_of_Amp --> 50
			lenStoreDict --> 149
			cisMappingFile --> full address to file to open to write to

	'''
	#vars
	line = of.readline()

	#make sure the write file doesn't already exist open write file
	if not os.path.exists(writeFile): 
		wf = open(writeFile, 'w')
	else:
		wf = open(writeFile, 'a')

	while line != '':
		pair = ''
		tabSplitLine = line.strip().split('\t')
		
		#get all read info
		xy, value = getAllInfoFromReadLine(tabSplitLine)

		#use divide and conquer to see if xy_read in store dictionary
		if findXYLoc(lenStoreDict, sortedStoreDictKeys, xy):

			pair = storeDict[xy]

		#filter reads close to each other
			if abs(pair[0] - value[0]) > 2000:
				#write pair to cis file
				#add value info chrnum, start postion, end position
				toWrite = '%s\t%s\t%s\t'%(value[4],value[0], value[-1])
				#add pair info
				toWrite += '%s\t%s\t%s\t'%(pair[4],pair[0], pair[-1])
				#add xy coors for each read
				toWrite += '%s\t%s\t%s\n'%(value[3],pair[3], xy)
				
				wf.write(toWrite)

		line = of.readline()

	#close files
	of.close()
	wf.close()
def deleteTempFiles(deleteDir):
	'''Purpose: delete entire directory tree
		(directoryLOC) --> dir deleted
	'''
	shutil.rmtree(deleteDir)
def findXYLoc(lenList, inputList, xy_read):
	'''purpose:  Find if xy_read is present in the inputList
		(int, list, str) --> bool
		Example input:
			lenList ---> 149
			inputList ---> [14222039, 14432056, 15502130, 15522223, 15742138, ...
			xy_read ---> 101482076
	'''
	j = 0
	k = lenList - 1
	found = False 

	#find if xy_read is at index zero
	if xy_read == inputList[j]:
		return True
	#find if xy_read is the last element of list
	elif xy_read == inputList[k]:
		return True
	#if xy_read larger than inputlist[-1] or smaller than inputlist[0] return False
	elif xy_read > inputList[-1] or xy_read < inputList[0]:
		return False
	#otherwise check to see by divide and conquer if Xy_read is in the list
	else:
		while k > j:
			averageIndex = (j + k)//2
			if (j+k)%2 == 1:
				averageIndex += 1
			#check to see if xy_read greater or less
			currentSearchVal = inputList[averageIndex]
			if xy_read > currentSearchVal:
				j = averageIndex
			elif xy_read < currentSearchVal:
				k = averageIndex
			elif xy_read == currentSearchVal:
				found = True
				j = k -1
				break
			if k - j == 1:
				k = j -1
		return found
def numProcessorsLocally(p):
	'''Purpose: Check how many processors are on this machine.  If p is greater than the number of processors on this machine set p to number of processors on this machine
		(int) --> int
	'''
	#check for number of proecessors
	numProcessors = mp.cpu_count()
	#if number of processors > than p set p to number of processors 
	if p > numProcessors:
		p = numProcessors
	#return p
	return p
def transPairInit(pairFolder, numProcessors):
	'''Purpose: Read each file in folder and see if that read is found in the storeDict.
		(folder address, int) --> Starts a pool.map
	'''
	
	#get all files in folder (save in a list to be an iterable to send to map)
	files = [pairFolder + f for f in os.listdir(pairFolder)]

	#send to a function to pair via map
	pool = mp.Pool(numProcessors)

	pool.map(transPair, files)
def transPair(fileAddress):
	'''Purpose: Pair reads of file to global pair_dict and save in global map_Reduce_Temp_Folder
		() --->
	'''

	#open input file
	of = open(fileAddress, 'r')
	
	#get file number and make sure it is not equal to chr of interest
	fileNum = fileAddress.split('/')[-1].split('.')[0]
	if fileNum != str(chro_num_interest):
		#get save file location
		saveFileLOC = '%s/trans/%s_paired.txt'%(map_Reduce_Temp_Folder, fileNum)

		#Send all files to the matchter
		readFileAndMatch(pair_dict, Dict_Keys, of, len_Dict, saveFileLOC)
def declareGlobals(args):
	'''Purpose: make global vars for MapReduce
		(list, str)-->globals
	'''
	global bin_size, first_bin, last_bin, total_num_bins, first, last, chro_num_interest, results_folder, file_dict

	bin_size = args.bin_size
	first = args.first
	last = args.last
	chro_num_interest = args.chroNumInterest

	first_bin = first/bin_size
	last_bin = last/bin_size
	total_num_bins = last_bin - first_bin

	results_folder, file_dict = makeFilesFolders(args, 2)
####################################################################################
#Make bining fucntions 
####################################################################################
def cisMapperFunction(fileLOC):
	'''
	Purpose: Open file iterate thru it bin all data on the same chromosome and between first and last with a bin size of bin_size
	  example of input line:
	      # chr6   129199686   129199721   chr6    113449625   113449660   97367740    97367740
	  Example output dictionary
	      # key: bin number of first pair
	      # value: bin number of second pair
	      # example: [('6_1586', '6_1680'), ('6_1493', '6_2625'), ('6_3697', '6_3693'), ('6_2525', '6_2499'), ('6_1960', '6_2516'), ('6_1145', '6_1566')]
   
	(str) --> dict
	'''
	#vars 
	mapOutputList = []

	#open file
	oRf = open(fileLOC, 'r')
	#read thru file
	line = oRf.readline()
	while line != '':
		#split up line
		tabSplitLine = line.split('\t')
		firstLoc = int(tabSplitLine[1])
		secondLoc = int(tabSplitLine[4])
		chrNum1 = tabSplitLine[0].replace('chr', '')
		chrNum2 = tabSplitLine[3].replace('chr', '')

		#make sure firstLoc is between global first and last and Loc of pair is also between global first and last
		if firstLoc >= first and firstLoc <= last and secondLoc >= first and secondLoc <= last:
			#get a bin number or key 
			key = (firstLoc//bin_size)-first_bin

			#get value bin number of second pair
			value =  (secondLoc//bin_size)-first_bin

			#convert to str and store first to ditgits with chromosome number
			strKey =  '%s_%s'%(chrNum1, key)
			strValue = '%s_%s'%(chrNum2, value)

			#append a tuple of (key, value) ot mapOutputList
			mapOutputList.append((strKey, strValue))

		line = oRf.readline()
	#once file is completed return mapOutput list to __call__

	return mapOutputList
def cisReducerFunction(item):
	'''Purpose: Take a list of bins inside a bin and count how many are in the bin.
		Input info:
			Key: bin number of first pair
			value: list of all values associated with that first bin
			Example Input: (6_1223, [6_1852, 6_2344, 6_2290, 6_2000, 6_2260, 6_2988, 6_1536, 6_1215, 6_2892, 6_2756, 6_1518, 6_3098, 
		output info:
			Write to cis file_dict
			Example line:
	'''
	#get max number in value
	currentValueList = item[1] #['6_1711', '6_2290', '6_1494', '6_3142', ...

	maxBinNum, minBinNumber = getMaxMinBinNum(currentValueList)

	#iterate from minBinNum to the length of the bin
	for i in range(minBinNumber, maxBinNum+1):
		#find how many times bin found in currentValueList
		toCount = '%s_%s'%(chro_num_interest, i)
		numBinInValue = currentValueList.count(toCount)

		#write to file if numBinInValue != 0
		if numBinInValue != 0:

			firstLOCWrite = item[0].split('_')[1]
			# if firstLOCWrite.startswith('T'):
			# 	print file_dict[str(chro_num_interest)]
			# else:
			firstLOCWrite = (int(firstLOCWrite) + first_bin) * bin_size
			secondLOCWrite = (i + first_bin) * bin_size
			toWrite = 'chr%s\t%s\tchr%s\t%s\t%s\n'%(chro_num_interest, firstLOCWrite, chro_num_interest, secondLOCWrite, numBinInValue)
			wf = open(file_dict['cis'], 'a')
			wf.write(toWrite)
			wf.close()
def getMaxMinBinNum(currentValueList):
	'''Purpose: find the min and max number of bins
		(list) --> int, int
	'''

	lenCurrentValue = len(currentValueList)
	maxBinNum = 0
	minBinNumber = int(currentValueList[0].split('_')[1])
	for i in range(0,lenCurrentValue):
		#if current bin number is larger than the maxBinNum make bin number equal to current bin number
		tempCurrentBinNumber = int(currentValueList[i].split('_')[1])
		if tempCurrentBinNumber > maxBinNum: 
			maxBinNum = int(currentValueList[i].split('_')[1])
		elif tempCurrentBinNumber < minBinNumber:
			minBinNumber = tempCurrentBinNumber

	return maxBinNum, minBinNumber
def transMapperFunction(fileLOC):
	''' Purpose: Open file and iterate thru it.  Bin all data. 
		Note binSize is set to a larger bin size

		Run var examples:
			chrNum, firstBP, lastBP, firstBin, lastBin, totalNumBins 1 0 195000000 0 65000 65000

			tabSplitLine, firstLoc, secondLoc, chrNum1, chrNum2
				['chr1', '121199366', '121199316', 'chr6', '128757119', '128757169', '26102017', '26102017', '26102017\n'] 121199366 128757119 1 6
				strKey, strValue 1_40399 6_1514

		(str) --> list
		Example input:
			../temp/mapReduceTempResults_20_30_27/trans/2_paired.txt
		file example:
			chr9	59145200	59145150	chr6	123777832	123777782	68052240	68052240	68052240
		Example output:
			[('2_8083', '6_42966'), ('2_14180', '6_43196')]
	'''
	#vars
	mapOutputList = []
	
	chrNum = int(fileLOC.split('/')[-1].split('_')[0])#get chr number

	#set all the binning vars for this chr
	binSize = 1000000
	firstBP = chr_Length_Dict[chrNum][0]
	lastBP = chr_Length_Dict[chrNum][1]

	firstBin = firstBP/ binSize
	lastBin = lastBP / binSize
	totalNumBins = lastBin - firstBin

	#open read file 
	oRf = open(fileLOC, 'r')
	line = oRf.readline()
	
	#iterate thru file
	while line != '':

		tabSplitLine = line.split('\t')
		firstLoc = int(tabSplitLine[1])
		secondLoc = int(tabSplitLine[4])
		chrNum1 = tabSplitLine[0].replace('chr', '')
		chrNum2 = tabSplitLine[3].replace('chr', '')
		
		#get a bin number of key
		key = (firstLoc//binSize) - firstBin

		#values binned on global values based on chromosome of interst
		#get value bin number of second pair
		value = (secondLoc//bin_size) -first_bin

		#convert to str and store first to ditgits with chromosome number
		strKey =  '%s_%s'%(chrNum1, key)
		strValue = '%s_%s'%(chrNum2, value)

		#append a tuple of (key, value) ot mapOutputList
		mapOutputList.append((strKey, strValue))

		line = oRf.readline()

	oRf.close()

	return mapOutputList
def transReducerFunction(item):
	'''Purpose: Take a tuple with a nested list and writes bed format to file with chrNum in file_dict
		(list) ---> 
		
		tabSplitLine, firstLoc, secondLoc, chrNum1, chrNum2
		['chr1', '121199366', '121199316', 'chr6', '128757119', '128757169', '26102017', '26102017', '26102017\n'] 121199366 128757119 1 6
		strKey, strValue 1_40399 6_1514

		reducer item ('1_40399', ['6_1514'])
		currentValueList ['6_1514']
		maxBinNum, minBinNumber 1514 1514
		firstBP, firstBin 0 0

		int(item[0].split('_')[1]) 40399
		firstLOCWrite 148199500 ---> this should be with in 500bp of 128757119
		first_bin, bin_size 256000 500

		i 1514
		secondLOCWrite 4542000 ----> this should be within 1000000bp of 121199366
		firstBin, binSize 0 1000000


		Input Example
			('2_42966', ['6_8083'])

	'''
	#get chr number
	chrNum = item[0].split('_')[0]

	#get max number in value
	currentValueList = item[1] #['6_1711', '6_2290', '6_1494', '6_3142', ...
	
	#get max and min bin number
	maxBinNum, minBinNumber = getMaxMinBinNum(currentValueList)

	#set all the binning vars for this chr
	binSize = 1000000
	firstBP = chr_Length_Dict[int(chrNum)][0]
	firstBin = firstBP/ binSize


	#iterate from minBinNum to the length of the bin
	for i in range(minBinNumber, maxBinNum+1):
		#find how many times bin found in currentValueList
		toCount = '%s_%s'%(chro_num_interest, i)
		numBinInValue = currentValueList.count(toCount)

		#write to file if numBinInValue != 0
		if numBinInValue != 0:
			#Convert bin back for chromsome of interest
			firstLOCWrite = (i + first_bin) * bin_size
			#Convert bin back to bp for varying chr
			secondLOCWrite = (int(item[0].split('_')[1]) + firstBin) * binSize
			toWrite = 'chr%s\t%s\tchr%s\t%s\t%s\n'%(chro_num_interest, firstLOCWrite, chrNum, secondLOCWrite, numBinInValue)
			wf = open(file_dict[chrNum], 'a')
			wf.write(toWrite)
			wf.close()		
####################################################################################
#Make Website
####################################################################################
def buildSharedData(outDataLOC, args, binSize):
	'''Purpose: Run all functions related to building sharedData.js
		(str, list, int) ---> call 3 functions
	# '''
	wf = open(outDataLOC + '/sharedData.js', 'a')
	print '\nRainbow HiC User Update:\n\tAdding Relevant Genes to visualization'
	
	getGeneData(outDataLOC, args, binSize, wf)
	#make wig file 
	if args.wigList != None:
		print '\nRainbow HiC User Update:\n\tAdding wig data to visualization'
		makeWigJson(outDataLOC, args, wf)

	#cut region of interest using enzyme_cutter
	hindiiiCutFile = makeCutFiles(args)

	# #Make bins from HindIII cut file
	binsList = makeHindiiiBins(outDataLOC, args, binSize, wf, hindiiiCutFile)	

	#close sharedData.js file
	wf.close()
	#return binsList for makeLineJson so lines can be added to bins
	return binsList	
def getGeneData(outDataLOC, args, binSize, wf):
	'''Purpose: Write to sharedData.js geneData info and windowSize var
		(str, list, int) ---> write to sharedData.js file genesLocJSON and windowSize vars
		Example to Write:
			var genesLocJSON=[{'name':'Tead4','geneNum':1,'start':128227143,'stop':128300813,'direction':'-'},
			var windowSize={'begin':128000000,'end':131000000,'chr':6,'samsamFile1':'current_run_files/R1.sam','samsamFile2':'current_run_files/R3.sam','hindiiiBinSize':1000};

	'''
	#open file to write to as appending
	
	
	#Write the genesLocJSON var
	wf.write('var genesLocJSON=[')

	#get all genes to remove
	if args.removeGenesFile != None:
		print 'removeing'
		removeGenesFileRegex = makeGeneRemoveRegEx(args.removeGenesFile)
	else:
		removeGenesFileRegex = False

	#open args.gtfFile as reading
	gf = open(args.gtfFile, 'r')
	line = gf.readline()
	
	#make a dict to save genes in
	saveGeneDict ={}
	
	#Iterate thru gtf file and find genes between first and last on chroNumInterest
	while line != '':
		#make var to keep track if the gene is between first and last an on chroNumInterest
		keepGene = True
		tabSplitLine = line.split('\t')
		
		#find chromosome number of the current line in Gtf 
		#if Chromosome number is not equalt to 		
		if tabSplitLine[0] == 'chr' + str(args.chroNumInterest):
			
			#make sure numbers are between first and last
			if int(tabSplitLine[3]) >= args.first and int(tabSplitLine[4]) < args.last:
				#get all of the info about line (last column)
				idInfo = tabSplitLine[-1].replace('\n', '').split(';')
			
				#search thru all the info for geneID
				for info in idInfo:
					#find which part of the info contains the gene id
					if 'gene_id' in info:
						geneIdAsIs = info.replace(' ', '').replace('gene_id', '').replace('"', '')
						geneId = re.sub('[^a-zA-Z0-9]', '', geneIdAsIs)

						#make sure gene is not in remove gene regex
						if removeGenesFileRegex != False:
							searchObj = re.search(removeGenesFileRegex, geneId)
							if searchObj:
								keepGene = False

				#if gene is not in remvoe list and geneId not in saveGeneDict add it to dict
				if keepGene and geneId not in saveGeneDict:
					#Add to dict in this order smallest loc, direction, largest loc 
					saveGeneDict[geneId] = [int(tabSplitLine[3]), tabSplitLine[6], int(tabSplitLine[4])]
				# if geneId already in dict update the largest LOC
				elif geneId in saveGeneDict:
					saveGeneDict[geneId][-1] = int(tabSplitLine[4])

		line = gf.readline()
	#write saveGeneDict to SharedData.js
	#sort gene dict and convert to a list
	sortedGeneDict = sorted(saveGeneDict.items(), key=operator.itemgetter(1))
	
	#keep track of gene number
	geneNum = 0
	for geneInfo in sortedGeneDict:
		#gene num makes genes stagered in image
		if geneNum >= 4:
			geneNum =1
		else:
			geneNum +=1
		#bulid what to write
		toWrite = '' 
		toWrite += "{name:'%s',"%(geneInfo[0])
		toWrite += "geneNum:%s,"%(geneNum)
		toWrite += "start:%s,"%(geneInfo[1][0])
		toWrite += "stop:%s,"%(geneInfo[1][2])
		toWrite += "direction:'%s'},"%(geneInfo[1][1])
		wf.write(toWrite)
	#close var
	wf.write('];\n')

	#add windowSize
	toWrite = "var windowSize={begin:%s,end:%s,chr:%s,samFile1:'%s',samFile2:'%s',hindiiiBinSize:%s, binSize: %s};\n"%(args.first, args.last, args.chroNumInterest, args.samFile1.split('/')[-1], args.samFile2.split('/')[-1], binSize, args.bin_size)
	wf.write(toWrite)
def makeGeneRemoveRegEx(removeGenesFile):
	'''Purpose: Make a string of regex to remove genes from gene list:

		(file address) --> regex str
		Example File input:
			AK	StartsWith
			Rik	EndsWith
			Klra18	Gene
		Example Ouput:
			'^AK|Rik$|Klra18'
	'''
	#open file
	of = open(removeGenesFile, 'r')
	line = of.readline()
	removeGenesFileRegex = ''		
	#Iterate thru file
	while line != '':
		tabSplitLine = line.split('\t')
		typeRegex = tabSplitLine[1].rstrip()
		#If column 2 == StartsWith put ^ in front
		if typeRegex == 'StartsWith':
			removeGenesFileRegex += '^%s|'%(tabSplitLine[0]) 
		#if Column 2 == Endswith put $ at end 
		elif typeRegex == 'EndsWith':
			removeGenesFileRegex += '%s$|'%(tabSplitLine[0]) 
		#if column 2 == Gene do nothing
		elif typeRegex == 'Gene':
			removeGenesFileRegex += '%s|'%(tabSplitLine[0]) 
		line = of.readline()
	
	of.close()
	return removeGenesFileRegex[0:-1]
def makeWigJson(outDataLOC, args, wf):
	'''Purpose:Write to sharedData.js file all wig info
		(str, list) -->
		Example Input:
			track type=wiggle_0 name="K27_mm10_normalized_chr6" description="K27_mm10_normalized_chr6" visibility=full color=6,92,29 priority=66 maxHeightPixels=50:50:11
			variableStep chrom=chr6 span=1
			3121475 10.1
			3121476 10.2
		Example output:
			var wigDataJSON=[{"fileName":"K27_mm10_normalized_chr6","fileType":"wiggle_0","DataSpectrum":[{"wL":128002648,"lH":13.39},{...
	'''
	#add wigDataJSon
	wf.write('var wigDataJSON=[')
	#load wig each wig file in args.wigList
	for wig in args.wigList:
		#open the file
		wigf = open(wig, 'r')
		line = wigf.readline()
		spaceSplitLine = line.split(' ')
		#find name and file type
		fileType = ''
		wigName = ''
		for item in spaceSplitLine:
			if 'type' in item:
				fileType = item.replace('type=', '')
			elif 'name' in item:
				wigName = item.replace('name=', '')
		#write data info sharedData.js
		toWrite = '{fileName:%s,fileType:"%s",DataSpectrum:['%(wigName, fileType)
		wf.write(toWrite)
		
		#vars
		maxY = 0.0
		countRange = 0 
		totalHeightOverRange = 0
		totalBpOverRange = 0
		rangeSize = 50

		# iterate over wig file to find data between args.first and args.last
		while line != '':
			spaceSplitLine = line.replace('\n', '').split(' ') #['4826189', '18.8\n']
			#make sure to skip header 
			if len(spaceSplitLine) == 2:	
				#find all data between args.first and args.lastBin
				if int(spaceSplitLine[0]) >= args.first and int(spaceSplitLine[0]) <= args.last:
					
					#find average of range of samples range size of rangeSize
					if countRange >= rangeSize:
						toWrite = '{wL:%s,lH:%s},'%(totalBpOverRange/rangeSize, totalHeightOverRange/rangeSize)
						wf.write(toWrite)	
						#reset vars
						countRange = 0
						totalBpOverRange = 0
						totalHeightOverRange = 0	

					else:
						countRange += 1
						totalHeightOverRange += float(spaceSplitLine[1])
						totalBpOverRange += int(spaceSplitLine[0])

					#find the highest Y of wig file
					if float(spaceSplitLine[1]) > maxY:
						maxY = float(spaceSplitLine[1])

					#stop loop once wig larger than args.last
					if int(spaceSplitLine[0]) >= args.last:
						break
			line = wigf.readline()

		#Write maxY to end of wig 
		toWrite = '],maxY:%s},'%(maxY)			
		wf.write(toWrite)
		wigf.close()

	wf.write('];\n')
def makeHindiiiBins(outDataLOC, args, binSize, wf, hindiiiCutFile):
	'''Purpose: Write to sharedData.js all hindiii site locations

		Also make bins and return it
		(str, list, int) ---> 
		
		Input file Example:
			128004570,HindIII,AAGCTT
		Output file Example:
			var hindiSites = [{"num":0,"loc":128004570},{"num":1,"loc":128005894...
	'''
	#open hindiii cut information
	of = open(hindiiiCutFile, 'r')
	line = of.readline()
	
	#write var to sharedData.js
	wf.write('var hindiSites=[')
	#bin info vars
	count = 0
	binsList = []

	while line != '':
		commaSplitLine = line.split(',')
		
		#get cut loc
		cutSite = int(line.split(',')[0])

		#makeSure cutSite is between args.first and args.last
		if cutSite >= args.first and cutSite <= args.last:

			#write cut site to sharedData.js
			toWrite = ''
			toWrite += '{num:%s,'%(count) 
			toWrite += 'loc:%s},'%(cutSite) 
			wf.write(toWrite)

			#get bin info 
			tempBin = [cutSite - binSize/2, cutSite + binSize/2, 'b%s'%(count), []]
			binsList.append(tempBin)
			count += 1
		line = of.readline()
	#close hindiSites var
	wf.write('];')

	#close files
	of.close()

	#return binsList for makeLineJson so lines can be added to bins
	return binsList
def makeCutFiles(args):
    	''' 	Purpose: Cut region of interest fasta sequence at hindiii sites
    		
    		(file) -> hindiii cut file
    	
    '''
    	#open fasta file
    
    	of = open(args.seqInterestRegion, 'r')
    	line = of.readline()

    	#vars
    	dna = ''
    	recognitionSite = 'AAGCTT'
    	length =  6
    	addNuclNum = 1

    	#save entire region of interest in one str
    	while line != '' and line != '\n':
    		#skip over lines starting with >
    		if not line.startswith('>'):
    			dna += line.rstrip().upper()
    		line = of.readline()

    	#make a file to save in 
    	hindiiiCutFile = '%s/temp/hindiiiCutFile.txt'%(args.o)
	wf = open(hindiiiCutFile, 'w')

	#scan dna and find hindiii cut sites and write to wf
	for i in range(len(dna)):
		current = dna[i:i+length]
		if len(current.rstrip()) == length and current == recognitionSite:
			wf.write(str(i+args.first+addNuclNum)+',HindIII,' + current + '\n')

	of.close()
	wf.close()
	return hindiiiCutFile
def makeLineJson(outDataLOC, args, binsList):
	'''Purpose: this function will read in a bed file and bins list and add the lines to a bin and output the line to JSON.  Write to the cisData.js file
		Example binsList input:
			[130987994, 130988994, 'bin960', []]
		Example hicGraphData var:
			var hicGraphData=[{"num":"id0","l1":128572500,"l2":128783000,"c":1},...
	'''
	#open cisData.js file
	wf = open(outDataLOC + '/cisData.js', 'a')
	
	#write the hicGraphData var to cisData.js file
	wf.write('var hicGraphData=[')
	
	#open cis bed file from tempBinnedResults example file name: 6and6_cis.txt
	cisF = open(file_dict['cis'], 'r')
	line = cisF.readline() #chr6	128572500	chr6	128783000	1

	#intiate vars
	count = 0
	maxIntensity = 0

	#interate over the cis file in tempBinnedResults named cisF
	while line != '':
		tabSplitLine = line.rstrip().split('\t')

		#start building pair for writing
		toWrite = '{num:"id%s",'%(count)
		toWrite += 'l1:%s,'%(tabSplitLine[1]) 
		toWrite += 'l2:%s,'%(tabSplitLine[3])
		toWrite += 'c:%s},'%(tabSplitLine[4]) 
		wf.write(toWrite)

		#find the maxIntensity for highest intensity for lines in website
		if int(tabSplitLine[4]) > maxIntensity:
			maxIntensity = int(tabSplitLine[4])

		#need to find what bin each loc in list falls into
		loc = [int(tabSplitLine[1]), int(tabSplitLine[3])]
		for tempLoc in loc:

			#iterate over binsList and find where tempLoc falls
			for i in range(len(binsList)):
				#rewrite as a divide and conqure method
				if tempLoc > binsList[i][0] and tempLoc < binsList[i][1]:
					binsList[i][-1].append('id%s'%(count))
					break


		#move on to next line
		count += 1
		line = cisF.readline()
	
	#finsih var hicGraphData
	wf.write('];')

	#close files
	wf.close()
	cisF.close()

	return binsList, maxIntensity
def makeBinJson(outDataLOC, binsList, maxIntensity):
	'''Purpose: Finish cisData.js file.  Write maxInensity to cisData.js. Write binLocations var to cisData.js

		binsList Example: 
			'bin578', ['id3']], [129848341, 129849341, 'bin579', ['id3']],
		
		maxIntensity Example: 
			var maxIntensity=381;
		binLocations Example:
			var binLocations=[{"bin":"bin152","binl1":128572189,"binl2":128573189, "binedLines":['id0']}

		(str, list, list, int) ---> 
	'''
	#open cisData.js file
	wf = open(outDataLOC + '/cisData.js', 'a')
	
	#write maxIntensity var cisData.js
	wf.write('\nvar maxIntensity=%s;\n'%(maxIntensity))

	#start binLocations var
	wf.write('var binLocations=[')

	#iterate over binsList
	for bini in binsList:

		#write to cisData.js file if bin list isn't empty
		if bini[-1] != []:
			toWrite = '{b:"%s",'%(bini[2])
			toWrite += 'l1:%s,'%(bini[0])
			toWrite += 'l2:%s, '%(bini[1])
			toWrite += 'bLs:%s},'%(bini[3])
			wf.write(toWrite)

	#finsih binLocations var
	wf.write('];\n')

	#close files
	wf.close()
def makeTransChrInfo(outDataLOC, args):
	'''Purpose: Write TransChrInfo
		Example transChrInfo:
			var transChrInfo={"chr1":{"begin":0,"end":195000000},
	'''
	wf = open(outDataLOC + '/transData.js', 'a')
	wf.write('var transChrInfo={')
	#open chromsomeFile
	of = open(args.chrSizeFile, 'r')
	line = of.readline()

	#iterate over file and write to transData file
	while line != '\n' and line != '':
		tabSplitLine = line.rstrip().split('\t')
		
		#make to write
		toWrite = '%s:'%(tabSplitLine[0].rstrip()) #chr
		toWrite += '{begin:%s,'%(tabSplitLine[1])
		toWrite += 'end:%s},'%(tabSplitLine[2])
		wf.write(toWrite)
		
		#advance to next line
		line = of.readline()

	#finsih var
	wf.write('};\n')
	#close files
	wf.close()
def makeTransLineJson(outDataLOC, args):
	'''Purpose: Make transHicGraphData and write it to transData.js
		(str, list) ---> write to transData.js

		trans Bed file example:
			chr6	129476500	chr14	139027000	1
		
		example transHicGraphData var
			var transHicGraphData={"chr11":[],"chr10":[{"num":"id0","bottomLoc":141746000,"topLoc":149563000,"c":1},],
	'''
	wf = open(outDataLOC + '/transData.js', 'a')
	wf.write('var transHicGraphData={')

	#iterate thru files in file_dict
	fileDictKeys = file_dict.keys() #Example: ['11', '10', '13', '12', '15', '14', '17', '16', '19', '18', 'cis', '1', '3', '2', '5', '4', '7', '6', '9', '8']

	#remove cis from file and remove chroNumInterst file
	fileDictKeys.remove('cis')
	fileDictKeys.remove(str(args.chroNumInterest))

	allMaxIntensities = []
	#read thru all files in tempBinnedresults
	for i in fileDictKeys:
		count = 0
		rf = open(file_dict[i], 'r')

		#start chr info
		wf.write('chr%s:['%(i))

		line = rf.readline()
		maxIntensity = 0
		pairCount = 0
		#read thru current read file in tempBinnedreuslts
		while line != '' and line != '\n':
			count += 1
			tabSplitLine = line.rstrip().split('\t')

			#start building pair for writing
			toWrite = '{num:"id%s",'%(pairCount)
			toWrite += 'bLoc:%s,'%(tabSplitLine[1]) 
			toWrite += 'tLoc:%s,'%(tabSplitLine[3])
			toWrite += 'c:%s},'%(tabSplitLine[4]) 
			wf.write(toWrite)
			
			if int(tabSplitLine[4]) > maxIntensity:
				maxIntensity = int(tabSplitLine[4])

			#get new line
			pairCount += 1
			line = rf.readline()
		print 'There are %s lines for chr %s'%(count, i)
		#add chrMaxIntesity to allMaxIntesities
		allMaxIntensities.append(maxIntensity)

		#add maxIntesnsity
		# wf.write('"chrMaxIntensity":%s'%(maxIntensity))

		#close chr list
		wf.write('],')

		rf.close()

	#close transHicGraphData
	wf.write('};\n')

	#add maxIntensity var
	wf.write('\nvar maxIntensity=%s;\n'%(max(allMaxIntensities)))

	#close files
	wf.close()
def progress_bar():
	"""
	progress looks like:
	Rainbow_Distiller progress:
		 [>...........................................................................] 0%
		Rainbow_Distiller progress:
		 [=========>.................................................................] 10%
		Rainbow_Distiller progress:
		 [====================>......................................................] 21%
		Rainbow_Distiller progress:
		 [===============================>...........................................] 32%
		Rainbow_Distiller progress:
		 [=========================================>.................................] 42%
		Rainbow_Distiller progress:
		 [====================================================>......................] 53%
		Rainbow_Distiller progress:
		 [===============================================================>...........] 64%
 
	"""
	global current_Progress, total_Steps
	screenWidth = 100
	current_Progress += 1
	percentProgress = (current_Progress*screenWidth)/total_Steps
	numEqualSigns = int((percentProgress-1)/1.5)
	numDots = int((screenWidth-percentProgress)/1.5)

	print "Rainbow_Distiller progress:\n [%s>%s] %s%%"%((numEqualSigns)*"=", (numDots)*".", percentProgress)
def arguments():
    	parser = argparse.ArgumentParser(description='Rainbow_Distiller analyzes Hi-C sequence data and automatically generates a localhost Rainbow HiC genome viewer website.')
    	parser.add_argument('--version', action='version', version=version)

    	#required
    	parser.add_argument('-s1f', '--samFile1', required=True, help='<file>  A sam formatted files obtained from two single read previously mapped to a reference genome. This file is required.')
    	parser.add_argument('-s2f', '--samFile2', required=True, help='<file>  A sam formatted files obtained from two single read previously mapped to a reference genome. This file is required.')
    	parser.add_argument('-cf', '--chrSizeFile', required=True, help='<file>  A tab seperated file containing chromsome number, start bp (normally 0), end bp. This file is required.')
    	parser.add_argument('-sf','--seqInterestRegion',required=True, help='<file>  A file of the sequence of the region of interest in fasta format. This file is required.')
    	parser.add_argument('-gf','--gtfFile' , required=True,help='<file>  A gtf file of the reference genome. This file is required.')

    	#integer inpu type
    	parser.add_argument('-f','--first',  required=True,nargs="?", const=1, type=int, help='<int>  Integer of first bp to be included in the region of interest. This argument is required.')
    	parser.add_argument('-l','--last',  required=True,nargs="?", const=1, type=int, help='<int>  Integer of last bp to be included in the region of interest. This argument is required.')
    	parser.add_argument('-c','--chroNumInterest',  required=True,nargs="?", type=int, help='<int>  Integer of chromosome of interest.  Currently any non integer chromsomes are not supported such as X, Y, or M. This argument is required.')
    
    	#optional
    	parser.add_argument('-name','--projectName', nargs='?', const=1, default='Rainbow_HiC_Results', help='<text>  An optional name to call the output folder.  (default: Rainbow_HiC_Results).')
    	parser.add_argument("-o", nargs="?", const=1, default=os.getcwd(), help='<folder>  A folder location were you would like to save the Rainbow HiC output folder.  Some temporary files will also be made here.  Make sure this location has at least the same amount of free space as the size of each of your sam files. (default: Working directory)')
    	parser.add_argument('-bin','--bin_size', nargs="?", type=int, const=1, default=500, help='<int>  An integer value of the read resolution of the data. (default: 500).')
    	parser.add_argument('-p', nargs="?", const=1,type=int, default=2, help='<int>  number of threads to launch (default: 2 ).')
    	parser.add_argument('-wig', '--wigList', nargs='+', required=False, help='<list of files>  A list of all the wig files to visualize in Rainbow HiC')
    	parser.add_argument('-rgf','--removeGenesFile', nargs='?', const=1, required=False,  help='<file> A tab seperated file of all the genes to remove from the visualization')

    	return parser
if __name__ == '__main__':

    	print """
            Rainbow HiC Developed by: Samantha Taffner
    -._    _.--'"`'--._    _.--'"`'--._    _.--'"`'--._    _   
        '-:`.'|`|"':-.  '-:`.'|`|"':-.  '-:`.'|`|"':-.  '.` : '.   
      '.  '.  | |  | |'.  '.  | |  | |'.  '.  | |  | |'.  '.:   '.  '.
      : '.  '.| |  | |  '.  '.| |  | |  '.  '.| |  | |  '.  '.  : '.  `.
      '   '.  `.:_ | :_.' '.  `.:_ | :_.' '.  `.:_ | :_.' '.  `.'   `.
             `-..,..-'       `-..,..-'       `-..,..-'       `         `

    	\n.... Starting processing .....\n"""  

    	progress_bar()
    	parser = arguments()
    	args = parser.parse_args()
    	main(args)
    
    	print '.... all done'
   	