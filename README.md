# **Rainbow Hic** 


# What is it?

> * Rainbow HiC is a pipeline for an easy analysis of Genome-Wide Chromosome Conformation Capture Data (Hi-C) datasets.
>
> * Static images of large Hi-C datasets provide very limiting analysis and interpretation abilities to researchers. Rainbow HIC allows researchers to interact with data, dynamically alter the view, or even resize or reposition the visualization in their browser window.   
>
> * It gives researchers with no bioinformatics background the ability to analyze their own data and visualizes them on a colorful Genome Viewer interface.   
>
> * In addition, Rainbow HIC has a built in functionality to export any resulting genome visualization in SVG format for easy inclusion in manuscripts or presentations.


# Pipeline Included Programs
> * HiCpy 
>> * A processing pipeline written in python that analyzes Hi-C sequence data 
> * Rainbow HiC Genome Viewer 
>> * An automatically generated localhost website which produces an interactive Hi-C Genome Viewer
>> * [Demo](http://taffners.bitbucket.org/index.html)

# How do you get set up? 
> ## How to Download Rainbow HiC?
> * Either go here and click on Download Repository:

    https://bitbucket.org/taffners/rainbowhic/downloads
> * Or use git to clone the repository (*type this in terminal*)
        
    git clone https://taffners@bitbucket.org/taffners/rainbowhic.git
> ## What else will you need to install?
> * Python 2 - https://www.python.org/download/
>> * Tested on python 2.7.6
> ## Hardware/Platform Requirements
> * HiCpy
>> * Platform: Python called from Command Line Interface
>> * Hardware: Larger datasets may have more robust requirements (Tested for sam files up to 5.3 GB each)
>> * RAM: At least 4GB (for -p 1 more RAM is necessary for parallel processing)
> * Rainbow Hic Genome Viewer 
>> * Platform: W3C Standards Compliant Web Browser (Chrome, Firefox, Safari, Internet Explorer 9+)
>>> * For larger datasets Chrome is considerably faster.   
>>>> * The creator of the very popular d3.js SVG library states that it is a firefox problem here: [link](https://github.com/mbostock/d3/issues/60#issuecomment-849451)

# File Formats

> ## **Sequence Alignment Map (Sam) file Format:**
>>
>> ## Requirements for file:
>> * _There must be two single read alignments of the data_
>> * File extension requirement
>>> * .sam
>> * Tab seperated file
>> * Query Name must have 6 colons
>
>> ## HiCpy Run Tag
>>> -s1f or -s2f
>
>> ## Where to get?
>> * We used bowtie1 to get the format:
>>> * [Bowtie manual](http://bowtie-bio.sourceforge.net/manual.shtml#sam-bowtie-output)
>
>> ## Example:
  	<QNAME> <FLAG> <RNAME> <POS> <MAPQ> <CIGAR> <MRNM> <MPOS> <ISIZE> <SEQ> <QUAL> [<TAG>:<VTYPE>:<VALUE> [...]]
  	HW-ST997:514:H8ULNADXX:1:1101:2413:2103	16	chr3	68031528	255	50M	*	0	0	GCGCACTAGTCATGAGGATTTCTTAACTGTGTTAACTGTGATAATTTCAT	GIIJJJIIIHIJHJJJJJJJIJJIJJIIIHJJIJJJJHHHHHFFFFFCC@	XA:i:0  MD:Z:50 NM:i:0	

> ## **CHR Size file Format:**
>
>> ## Requirements for file:
>> * tab seperated file
>> * this file requries 3 columns
>> * try to avoid any white space in this file
>> * What to include in each column?
	>>> * chr#, start coordinate in bp, ending coordinate in bp 
>> * this program currently only works on interger chromsomes
>
>> ## HiCpy Run Tag
>>> -cf
>
>> ## Where to get?
>> * I went to [NCBI genome](http://www.ncbi.nlm.nih.gov/genome?term=mus%20musculus) and typed mus musculus[orgn] it also works with human[orgn]
>
>> ## Example (mus musculus):
  	<chr#>	<start_bp>	<end_bp>
  	chr1	0	195470000
  	chr2	0	182110000
  	chr3	0	160400000
  	chr4	0	156510000
  	chr5	0	151840000
  	chr6	0	149740000
  	chr7	0	145440000
  	chr8	0	129400000
  	chr9	0	124600000
  	chr10	0	130700000
  	chr11	0	122080000
  	chr12	0	120130000
  	chr13	0	120420000
  	chr14	0	124900000
  	chr15	0	104040000
  	chr16 	0	98210000
  	chr17	0	94990000
  	chr18 	0	90700000
  	chr19	0	61430000

>> __Make sure if you copy the above information you retab the file and remove the column headers__

> ## **Fasta formated sequence of region of interest**
>
>> ## Requirements for file:
>> * Make sure the file ends with .fa
> * __Make sure the file contains the entire region of interest and only the region of interest__ 
>
>> ## HiCpy Run Tag
>>> -sf
>
>> ## Where to get?
>> * Whole chromosomes can be found here
>>> * http://hgdownload.cse.ucsc.edu/downloads.html
>> * For segments of genome (I've tried up to 1MB it might work for larger segments)
>>> * Go to [UCSC](https://genome.ucsc.edu/cgi-bin/hgGateway) and get the region of interest in genome the of interest
>>> * After region loads go to view > DNA
>>> * On Next page push get DNA (__NOTE DO NOT MASK REPEATS__)
>>> * Make sure you wait until all data loads.  This could take a few minutes.
>>> * Copy entire sequence (on linux or windows push Ctrl A)(on mac push apple A)
>>> * Paste into your text editor of choice and save this file with a .fa extension 
>>>> * Examples of cross platform Text editors: my favorite [Sublime](https://www.sublimetext.com/), [Atom](https://atom.io/), or [Brackets](http://brackets.io/) 
>>>> * There are platform specific text editors also such as TextWrangler or textmate for OS X
>>>> * You can choose any text editor of your choice.  Just make sure you do not use any word processing software.

> ## **gtf File:**
>
>> ## Requirements for file:
>> * A tab seperated file
>> * The gene_id will be the name of the gene on the Rainbow HiC genome viewer
>
>> ## Where to get?
>
>> ## HiCpy Run Tag
>>> -gf
>
>> ## Example:
	 chr1	unknown	exon	3214482	3216968	.	-	.	gene_id "Xkr4"; gene_name "Xkr4"; p_id "P15119"; transcript_id "NM_001011874"; tss_id "TSS26513";
		
> ## **Remove gene File:**
>
>> ## Requirements for file:
>> * A tab seperated file
>> * Two columns
>> * Include no whitespace
>> * First column contains Gene or pattern to remove
>> * Second column contains one of the following:
>>> * StartsWith - Use to remove all genes which start with what is in column 1
>>> * EndsWith - Use to remove all genes which end with what is in column 1
>>> * Gene - Remove the gene which matches column 1 exactly 
>
>> ## HiCpy Run Tag
>>> -rgf
>
>> ## Where to get?
>> * You will have to make this file but it is not a required file
>
>> ## Example:
		Mir     StartsWith
		Rik     EndsWith
		Klra18  Gene

>> __Make sure if you copy the above information you retab the file__

# How to run

> ## Open terminal and navigate to folder
> Example:
>> $ cd rainbowhic
> ## The following files should be in this folder
> $ ls 
> 
> HiCpy.py  README.md  Step3Tools
> ## Run HiCpy from within this folder.  
> * Usage of HiCpy is outlined below.  
> * The help menu can also be visualized in terminal by;
>> $ python HiCpy.py -h
>
> ## After HiCpy completes 
> * Naviagate the output folder and double click on the index file
> * This should open Rainbow HiC in your default internet browser.  
> * Rainbow HiC works best in Chrome internet Browser.   

> ## Usage: 
	python HiCpy.py -s1f SAMFILE1 -s2f SAMFILE2 -cf CHRSIZEFILE
                -sf SEQINTERESTREGION -gf GTFFILE -f [FIRST] -l [LAST] -c
                [CHRONUMINTEREST] [-name [PROJECTNAME]] [-o [O]]
                [-bin [BIN_SIZE]] [-p [P]] [-wig WIGLIST [WIGLIST ...]]
                [-rgf [REMOVEGENESFILE]]

> __Note 1: HiCpy contains no positional arguments all arguments require a tag__
> __Note 2: HiCpy requires 8 arguments__

> ## Arguments:
  	-s1f SAMFILE1, --samFile1 SAMFILE1
                        <file> A sam formatted files obtained from two single
                        read previously mapped to a reference genome. This
                        file is required.
    >
  	-s2f SAMFILE2, --samFile2 SAMFILE2
                        <file> A sam formatted files obtained from two single
                        read previously mapped to a reference genome. This
                        file is required.
    >
  	-cf CHRSIZEFILE, --chrSizeFile CHRSIZEFILE
                        <file> A tab seperated file containing chromsome
                        number, start bp (normally 0), end bp. This file is
                        required.
    >
  	-sf SEQINTERESTREGION, --seqInterestRegion SEQINTERESTREGION
                        <file> A file of the sequence of the region of
                        interest in fasta format. This file is required.
    >
  	-gf GTFFILE, --gtfFile GTFFILE
                        <file> A gtf file of the reference genome. This file
                        is required.
    >
  	-f [FIRST], --first [FIRST]
                        <int> Integer of first bp to be included in the region
                        of interest. This argument is required.
                        Example for starting bp: -f 128000000
    >
  	-l [LAST], --last [LAST]
                        <int> Integer of last bp to be included in the region
                        of interest. This argument is required.
                        Example for ending bp: -l 130000000
    >
    -c [CHRONUMINTEREST], --chroNumInterest [CHRONUMINTEREST]
                      <int> Integer of chromosome of interest. Currently any
                      non integer chromsomes are not supported such as X, Y,
                      or M. This argument is required.
                      Example for chromsome 6: -c 6

> ## Optional Arguments:
	-h, --help            show this help message and exit
  >
	--version             show program's version number and exit
  >
	-name [PROJECTNAME], --projectName [PROJECTNAME]
                      <text> An optional name to call the output folder.
                      (default: Rainbow_HiC_Results).
  >
	-o [O]             <folder> A folder location were you would like to save
                      the Rainbow HiC output folder. Some temporary files
                      will also be made here. Make sure this location has at
                      least the same amount of free space as the size of
                      each of your sam files. (default: Working directory)
  >
	-bin [BIN_SIZE], --bin_size [BIN_SIZE]
                      <int> An integer value of the read resolution of the
                      data. (default: 500).
  >
	-p [P]              <int> number of threads to launch (default: 2 ).
  >
	-wig WIGLIST [WIGLIST ...], --wigList WIGLIST [WIGLIST ...]
                      <list of files> A list of all the wig files to
                      visualize in Rainbow HiC
  >
	-rgf [REMOVEGENESFILE], --removeGenesFile [REMOVEGENESFILE]
                      <file> A tab seperated file of all the genes to remove
                      from the visualization


> ## How to use supplied sample files:
    python Rainbow_Distiller.py -s1f Sample_Run_files_Rainbow_Disillter/Sample_Data_R1.sam -s2f Sample_Run_files_Rainbow_Disillter/Sample_Data_R3.sam -cf Sample_Run_files_Rainbow_Disillter/mm10_CHR_Size_file.txt -sf Sample_Run_files_Rainbow_Disillter/Region_of_interest_chr6_120000000_128000000_mm10.fa -gf Sample_Run_files_Rainbow_Disillter/genes_mm10.gtf -f 120000000 -l 128000000 -c 6 -rgf Sample_Run_files_Rainbow_Disillter/removeGenes.txt



# Authors
> * Samantha Taffner - Software development
> * Beatrice Plougastel - Idea Generator and Software Testing
> * Wayne Yokoyama - Principal Investigator

# Bug Reporting
> Send an email to Samantha Taffner at staffner@dom.wustl.edu

## Libraries used:

>> * Python - GNU General Public License
>> * Javascipt - GNU General Public License
>> * JQuery - MIT Open Source License
>> * JQuery UI - MIT Open Source License
>> * Bootstrap - MIT Open Source License
>> * Bootstrap Tour - Apache Open Source License
>> * Spectrum JS - GNU General Public License